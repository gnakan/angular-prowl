var app = angular.module('Prowl', ["firebase"]); //create the module

app.run(function($rootScope){
	$rootScope.$on('handleMessage', function(event, args){
		$rootScope.$broadcast('handleBroadcast', args);
	});
});


//event controller
function ProwlController($scope, $firebase){
	var ref = new Firebase("https://sizzling-fire-892.firebaseio.com/sprint-log/"); //bind the model to firebase location

	$scope.messages = $firebase(ref);
	$scope.incidents = [
		{
    		"id": "inc01",
    		"name": "added a story to the product backlog for ",
    		"points": 5
    	},
    	{
    		"id": "inc02",
    		"name": "made sprint request with due date in middle of sprint for",
    		"points": -5
    	}
	];

	$scope.projects = [
        {
            "id": "proj001",
            "name": "Tagstack",
            "po": "Julia",
            "gender": "female",
            "points": 0
        },
        {
            "id": "proj002",
            "name": "Video UGC Upload",
            "po": "Sherry",
            "gender": "female",
            "points": 0
        },
        {
            "id": "proj003",
            "name": "iOS Sports",
            "po": "Todd Kelly",
            "gender": "male",
            "points": 0
        }
    ];
    
    $scope.$on('handleBroadcast', function(event, args){
    	$scope.buildBoard();
    });

    $scope.buildBoard = function(){
    	
    	var scoreList = $scope.projects;

    	//reset the score values because we are going to recalculate again
    	angular.forEach(scoreList, function(scoreData){
	    	scoreData.points = 0;
    	});

    	$scope.messages.$on('loaded', function(){
    		var keys = $scope.messages;
    		angular.forEach(keys, function(data){
    			if(typeof data == 'object')
    			{
    				var project = data.id;
    				var score = data.value;
    				//update the scoreboard
    				angular.forEach(scoreList, function(scoreData){
	    				if(scoreData.id == project)
	    				{
	    					scoreData.points = scoreData.points + score;
	    				}
    				});
    			}
    		
    		});
    	});
    };


	$scope.addMessage = function(e){
		var messageToSend = {
			productOwner: $scope.project.po, 
			type: $scope.incident.name, 
			value: $scope.incident.points, 
			gender: $scope.project.gender, 
			project: $scope.project.name,
			id: $scope.project.id
		};
		
		$scope.messages.$add(messageToSend);
		$scope.productOwner = "";
		$scope.type = ""; //clear the type field
		$scope.$emit('handleMessage', messageToSend);
	};

	$scope.updateForm = function(){
		console.log('update form');
	};
};

//config controller
function ConfigController($scope, configService){
	$scope.data = null;

	configService.getData(function(dataResponse){
		$scope.data = dataResponse;
	});
};

//gets the data from the config
app.service('configService', function($http){
	
	this.getData = function(callBackFunc){
		$http.get('config.json')
			.success(function(data){
				callBackFunc(data);
			});
	};
});